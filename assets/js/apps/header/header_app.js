CoinManager.module("HeaderApp", function(Header, CoinManager, Backbone, Marionette, $, _){
  var API = {
    listHeader: function(){
      Header.List.Controller.listHeader();
    }
  };

  CoinManager.commands.setHandler("set:active:header", function(name){
    CoinManager.HeaderApp.List.Controller.setActiveHeader(name);
  });

  Header.on("start", function(){
    API.listHeader();
  });
});
