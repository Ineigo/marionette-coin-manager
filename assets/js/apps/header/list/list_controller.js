CoinManager.module("HeaderApp.List", function(List, CoinManager, Backbone, Marionette, $, _){
  List.Controller = {
    listHeader: function(){
      var links = CoinManager.request("header:entities");
      var headers = new List.Headers({collection: links});

      headers.on("brand:clicked", function(){
        CoinManager.trigger("coins:list");
      });

      headers.on("childview:navigate", function(childView, model){
        var trigger = model.get("navigationTrigger");
        CoinManager.trigger(trigger);
      });

      CoinManager.regions.header.show(headers);
    },

    setActiveHeader: function(headerUrl){
      var links = CoinManager.request("header:entities");
      var headerToSelect = links.find(function(header){ return header.get("url") === headerUrl; });
      headerToSelect.select();
      links.trigger("reset");
    }
  };
});
