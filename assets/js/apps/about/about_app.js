CoinManager.module("AboutApp", function(AboutApp, CoinManager, Backbone, Marionette, $, _){
  AboutApp.Router = Marionette.AppRouter.extend({
    appRoutes: {
      "about" : "showAbout"
    }
  });

  var API = {
    showAbout: function(){
      AboutApp.Show.Controller.showAbout();
      CoinManager.execute("set:active:header", "about");
    }
  };

  CoinManager.on("about:show", function(){
    CoinManager.navigate("about");
    API.showAbout();
  });

  AboutApp.on("start", function(){
    new AboutApp.Router({
      controller: API
    });
  });
});
