CoinManager.module("AboutApp.Show", function(Show, CoinManager, Backbone, Marionette, $, _){
  Show.Message = Marionette.ItemView.extend({
    template: "#about-message"
  });
});
