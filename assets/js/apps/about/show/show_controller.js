CoinManager.module("AboutApp.Show", function(Show, CoinManager, Backbone, Marionette, $, _){
  Show.Controller = {
    showAbout: function(){
      var view = new Show.Message();
      CoinManager.regions.main.show(view);
    }
  };
});
