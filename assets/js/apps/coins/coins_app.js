CoinManager.module("CoinsApp", function(CoinsApp, CoinManager, Backbone, Marionette, $, _){
  CoinsApp.Router = Marionette.AppRouter.extend({
    appRoutes: {
      "coins(/filter/criterion::criterion)": "listCoins",
      "coins/:id": "showCoin",
      "coins/:id/edit": "editCoin"
    }
  });

  var API = {
    listCoins: function(criterion){
      CoinsApp.List.Controller.listCoins(criterion);
      CoinManager.execute("set:active:header", "coins");
    },

    showCoin: function(id){
      CoinsApp.Show.Controller.showCoin(id);
      CoinManager.execute("set:active:header", "coins");
    },

    editCoin: function(id){
      CoinsApp.Edit.Controller.editCoin(id);
      CoinManager.execute("set:active:header", "coins");
    }
  };

  CoinManager.on("coins:list", function(){
    CoinManager.navigate("coins");
    API.listCoins();
  });

  CoinManager.on("coins:filter", function(criterion){
    if(criterion){
      CoinManager.navigate("coins/filter/criterion:" + criterion);
    }
    else{
      CoinManager.navigate("coins");
    }
  });

  CoinManager.on("coin:show", function(id){
    CoinManager.navigate("coins/" + id);
    API.showCoin(id);
  });

  CoinManager.on("coin:edit", function(id){
    CoinManager.navigate("coins/" + id + "/edit");
    API.editCoin(id);
  });

  CoinsApp.on("start", function(){
    new CoinsApp.Router({
      controller: API
    });
  });
});
