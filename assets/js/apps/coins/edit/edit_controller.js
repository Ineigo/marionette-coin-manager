CoinManager.module("CoinsApp.Edit", function(Edit, CoinManager, Backbone, Marionette, $, _){
  Edit.Controller = {
    editCoin: function(id){
      var loadingView = new CoinManager.Common.Views.Loading({
        title: "Исскуственная задержка от сервера",
        message: "Загрузка данных с задержкой, что-бы продемострировать экран подгрузки"
      });
      CoinManager.regions.main.show(loadingView);

      var fetchingCoins = CoinManager.request("coin:entity", id);
      $.when(fetchingCoins).done(function(coin){
        var view;
        if(coin !== undefined){
          view = new Edit.Coin({
            model: coin,
            generateTitle: true
          });

          view.on("form:submit", function(data){
            if(coin.save(data)){
              CoinManager.trigger("coin:show", coin.get("id"));
            }
            else{
              view.triggerMethod("form:data:invalid", coin.validationError);
            }
          });
        }
        else{
          view = new CoinManager.CoinsApp.Show.MissingCoin();
        }

        CoinManager.regions.main.show(view);
      });
    }
  };
});
