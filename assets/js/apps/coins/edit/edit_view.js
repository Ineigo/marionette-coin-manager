CoinManager.module("CoinsApp.Edit", function(Edit, CoinManager, Backbone, Marionette, $, _){
  Edit.Coin = CoinManager.CoinsApp.Common.Views.Form.extend({
    initialize: function(){
      this.title = "Редактирование " + this.model.get("name") + " - " + this.model.get("count") + "шт.";
    },

    onRender: function(){
      if(this.options.generateTitle){
        var $title = $('<h1>', { text: this.title });
        this.$el.prepend($title);
      }

      this.$(".js-submit").text("Применить изменения");
    }
  });
});
