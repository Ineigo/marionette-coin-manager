CoinManager.module("CoinsApp.Show", function(Show, CoinManager, Backbone, Marionette, $, _){
  Show.Controller = {
    showCoin: function(id){
      var loadingView = new CoinManager.Common.Views.Loading({
        title: "Исскуственная задержка от сервера",
        message: "Загрузка данных с задержкой, что-бы продемострировать экран подгрузки"
      });
      CoinManager.regions.main.show(loadingView);

      var fetchingContact = CoinManager.request("coin:entity", id);
      $.when(fetchingContact).done(function(contact){
        var coinView;
        if(contact !== undefined){
          coinView = new Show.Coin({
            model: contact
          });

          coinView.on("coin:edit", function(coin){
            CoinManager.trigger("coin:edit", coin.get("id"));
          });
        }
        else{
          coinView = new Show.MissingCoin();
        }

        CoinManager.regions.main.show(coinView);
      });
    }
  }
});
