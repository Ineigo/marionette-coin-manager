CoinManager.module("CoinsApp.Show", function(Show, CoinManager, Backbone, Marionette, $, _){
  Show.MissingCoin = Marionette.ItemView.extend({
    template: "#missing-coin-view"
  });

  Show.Coin = Marionette.ItemView.extend({
    template: "#coin-view",

    events: {
      "click a.js-edit": "editClicked"
    },

    editClicked: function(e){
      e.preventDefault();
      this.trigger("coin:edit", this.model);
    }
  });
});
