CoinManager.module("CoinsApp.List", function(List, CoinManager, Backbone, Marionette, $, _){
  List.Controller = {
    listCoins: function(criterion){
      var loadingView = new CoinManager.Common.Views.Loading();
      CoinManager.regions.main.show(loadingView);

      var fetchingCoins = CoinManager.request("coin:entities");

      var coinsListLayout = new List.Layout();
      var coinsListPanel = new List.Panel();

      $.when(fetchingCoins).done(function(coins){
        var filteredCoins = CoinManager.Entities.FilteredCollection({
          collection: coins,
          filterFunction: function(filterCriterion){
            var criterion = filterCriterion.toLowerCase();
            return function(coin){
              if(coin.get("name").toLowerCase().indexOf(criterion) !== -1
                || coin.get("nominal").toLowerCase().indexOf(criterion) !== -1
                || coin.get("year").toLowerCase().indexOf(criterion) !== -1
                || coin.get("country").toLowerCase().indexOf(criterion) !== -1){
                  return coin;
              }
            };
          }
        });

        if(criterion){
          filteredCoins.filter(criterion);
          coinsListPanel.once("show", function(){
            coinsListPanel.triggerMethod("set:filter:criterion", criterion);
          });
        }

        var coinsListView = new List.Coins({
          collection: filteredCoins
        });

        coinsListPanel.on("coins:filter", function(filterCriterion){
          filteredCoins.filter(filterCriterion);
          coinsListLayout.previewRegion.reset();
          CoinManager.trigger("coins:filter", filterCriterion);
        });

        coinsListLayout.on("show", function(){
          coinsListLayout.panelRegion.show(coinsListPanel);
          coinsListLayout.coinsRegion.show(coinsListView);
        });

        coinsListPanel.on("coin:new", function(){
          var newCoin = new CoinManager.Entities.Coin();

          var view = new CoinManager.CoinsApp.New.Coin({
            model: newCoin
          });

          view.on("form:submit", function(data){
            if(coins.length > 0){
              var highestId = coins.max(function(c){ return c.id; }).get("id");
              data.id = highestId + 1;
            }
            else{
              data.id = 1;
            }
            if(newCoin.save(data)){
              coins.add(newCoin);
              view.trigger("dialog:close");
              var newCoinView = coinsListView.children.findByModel(newCoin);
              // check whether the new coin view is displayed (it could be
              // invisible due to the current filter criterion)
              if(newCoinView){
                newCoinView.flash("success");
              }
            }
            else{
              view.triggerMethod("form:data:invalid", newCoin.validationError);
            }
          });

          CoinManager.regions.dialog.show(view);
        });

        coinsListView.on("childview:coin:show", function(childView, args){
          CoinManager.trigger("coin:show", args.model.get("id"));
        });

        coinsListView.on("childview:coin:edit", function(childView, args){
          var model = args.model;
          var view = new CoinManager.CoinsApp.Edit.Coin({
            model: model
          });

          view.on("form:submit", function(data){
            if(model.save(data)){
              childView.render();
              view.trigger("dialog:close");
              childView.flash("success");
            }
            else{
              view.triggerMethod("form:data:invalid", model.validationError);
            }
          });

          CoinManager.regions.dialog.show(view);
        });

        coinsListView.on("childview:coin:select", function (childView, args) {
          var model = args.model;

          var view = new List.CoinPreview({
            model: model
          });

          coinsListLayout.previewRegion.show(view);
        });

        coinsListView.on("childview:coin:delete", function(childView, args){
          args.model.destroy();
          coinsListLayout.previewRegion.reset();
        });

        CoinManager.regions.main.show(coinsListLayout);
      });
    }
  }
});
