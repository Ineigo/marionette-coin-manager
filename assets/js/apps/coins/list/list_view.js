CoinManager.module("CoinsApp.List", function(List, CoinManager, Backbone, Marionette, $, _){
  List.Layout = Marionette.LayoutView.extend({
    template: "#coins-list-layout",

    regions: {
      panelRegion: "#panel-region",
      coinsRegion: "#coins-region",
      previewRegion: "#preview-region"
    }
  });

  List.Panel = Marionette.ItemView.extend({
    template: "#coins-list-panel",

    triggers: {
      "click button.js-new": "coin:new"
    },

    events: {
      "submit #filter-form": "filterCoins"
    },

    ui: {
      criterion: "input.js-filter-criterion"
    },

    filterCoins: function(e){
      e.preventDefault();
      var criterion = this.$(".js-filter-criterion").val();
      this.trigger("coins:filter", criterion);
    },

    onSetFilterCriterion: function(criterion){
      this.ui.criterion.val(criterion);
    }
  });

  List.Coin = Marionette.ItemView.extend({
    tagName: "tr",
    className: "pointer",
    template: "#coins-list-item",

    triggers: {
      "click td a.js-show": "coin:show",
      "click td a.js-edit": "coin:edit",
      "click button.js-delete": "coin:delete",
      "click": "coin:select"
    },

    events: {
      "click": "highlightName"
    },

    flash: function(cssClass){
      var $view = this.$el;
      $view.hide().toggleClass(cssClass).fadeIn(800, function(){
        setTimeout(function(){
          $view.toggleClass(cssClass)
        }, 500);
      });
    },

    highlightName: function(e){
      this.$el.addClass("warning");
      this.$el.siblings().removeClass("warning");
    },

    remove: function(){
      var self = this;
      this.$el.fadeOut(function(){
        Marionette.ItemView.prototype.remove.call(self);
      });
    }
  });

  List.CoinPreview = Marionette.ItemView.extend({
    template: "#coins-list-preview"
  });

  var NoCoinsView = Marionette.ItemView.extend({
    template: "#coins-list-none",
    tagName: "tr",
    className: "alert"
  });

  List.Coins = Marionette.CompositeView.extend({
    tagName: "table",
    className: "table table-hover",
    template: "#coins-list",
    emptyView: NoCoinsView,
    childView: List.Coin,
    childViewContainer: "tbody",

    initialize: function(){
      this.listenTo(this.collection, "reset", function(){
        this.attachHtml = function(collectionView, childView, index){
          collectionView.$el.append(childView.el);
        }
      });
    },

    onRenderCollection: function(){
      this.attachHtml = function(collectionView, childView, index){
        collectionView.$el.prepend(childView.el);
      }
    }
  });
});
