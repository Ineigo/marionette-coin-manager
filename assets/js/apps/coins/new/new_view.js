CoinManager.module("CoinsApp.New", function(New, CoinManager, Backbone, Marionette, $, _){
  New.Coin = CoinManager.CoinsApp.Common.Views.Form.extend({
    title: "Новая монета",

    onRender: function(){
      this.$(".js-submit").text("Добавить");
    }
  });
});
