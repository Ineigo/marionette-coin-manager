CoinManager.module("Entities", function(Entities, CoinManager, Backbone, Marionette, $, _){
  var error_messages = {
    empty: "Нужно заполнить",
    short: "Слишком мало символов"
  }
  Entities.Coin = Backbone.Model.extend({
    urlRoot: "coins",

    defaults: {
      name: "",
      nominal: "",
      count: "",
      year: "",
      country: "",
      imgUrl: ""
    },

    validate: function(attrs, options) {
      var errors = {}
      if (! attrs.nominal) {
        errors.nominal = error_messages.empty;
      }
      if(! attrs.year) {
        errors.year = error_messages.empty;
      }
      if(! attrs.country) {
        errors.country = error_messages.empty;
      }
      if (! attrs.name) {
        errors.name = error_messages.empty;
      }
      else{
        if (attrs.name.length < 2) {
          errors.name = error_messages.short;
        }
      }
      if( ! _.isEmpty(errors)){
        return errors;
      }
    }
  });

  Entities.configureStorage("CoinManager.Entities.Coin");

  Entities.CoinCollection = Backbone.Collection.extend({
    url: "coins",
    model: Entities.Coin,
    comparator: "name"
  });

  Entities.configureStorage("CoinManager.Entities.CoinCollection");

  var initializeCoins = function(){
    var coin = new Entities.CoinCollection([
      { id: 1, name: "Десять рублей", nominal: "10р.", year: "2013", country: "Россия", count: "100", imgUrl: "http://aeol.su/sites/default/files/fornews/10r_2013mmd_2.2a.jpg" },
      { id: 2, name: "Пять рублей", nominal: "5р.", year: "1997", country: "Россия", count: "40", imgUrl: "http://rus-coins.ru/wp-content/uploads/2012/12/5-rub-1997-goda-spmd.jpg" },
      { id: 3, name: "Один рубль", nominal: "1р.", year: "2001-2016", country: "Россия", count: "1000", imgUrl: "http://www.tomovl.ru/money/images/money_Russia_1997_1ryble2001.jpg" }
    ]);
    coin.forEach(function(coin){
      coin.save();
    });
    return coin.models;
  };

  var API = {
    getCoinEntities: function(){
      var coins = new Entities.CoinCollection();
      var defer = $.Deferred();
      coins.fetch({
        success: function(data){
          defer.resolve(data);
        }
      });
      var promise = defer.promise();
      $.when(promise).done(function(fetchedCoins){
        if(fetchedCoins.length === 0){
          // if we don't have any coins yet, create some for convenience
          var models = initializeCoins();
          coins.reset(models);
        }
      });
      return promise;
    },

    getCoinEntity: function(coinId){
      var coin = new Entities.Coin({id: coinId});
      var defer = $.Deferred();
      setTimeout(function(){
        coin.fetch({
          success: function(data){
            defer.resolve(data);
          },
          error: function(data){
            defer.resolve(undefined);
          }
        });
      }, 2000);
      return defer.promise();
    }
  };

  CoinManager.reqres.setHandler("coin:entities", function(){
    return API.getCoinEntities();
  });

  CoinManager.reqres.setHandler("coin:entity", function(id){
    return API.getCoinEntity(id);
  });
});
